# !/usr/bin/env python
# -*- coding:utf-8 -*-


"""
    RFM分析器:
        ・売上データを読み込み,RFM分析による顧客ごとR,F,M値とRFM値をcsv出力する
        ・売上データを読み込み,RFM分析による顧客ごとR,F,M値とRFM値をDBへ格納する
        ・売上データを読み込み,RFM分析による顧客ごとR,F,M値から相対位置を3次元プロットする
            入力: 月毎の売上データ(csv形式)
                index: ID
                column: 日付
            出力: R,F,M値およびRFM値
                Recency: 基準日からさかのぼった直近で顧客全体の売上変化率を上回るまでの期間
                    ←サブスクリプションモデルへ適用するため従来のRecencyの定義を変更
                Frequency: 合計契約回数
                Monetary: 売上合計額
                R-value: 閾値を基準とする1〜9までの整数
                F-value: 閾値を基準とする1〜9までの整数
                M-value: 閾値を基準とする1〜9までの整数
                RFM-value: R,F,Mの値を合計した値
    python --version: Python 3.7.2
    created: 2019-09-18
    Auther: S.TAKEHARA
"""

import pandas as pd
import numpy as np
import datetime as dt
from sqlalchemy import create_engine


def _read_csv():
    # 元データ読み込み
    df = pd.read_csv('bare_sales_.csv', comment='#')
    # カラム名変更
    df = df.rename(columns={'Unnamed: 0': 'uid'})
    # 欠損値を0に置換
    df = df.fillna(0)
    return df


# 変化率を求める
def _rate_of_change(df):
    # 転置
    df = df.T
    # 0行目をヘッダ名にする
    df.columns = df.iloc[0]
    # 'uid'行を削除する
    df = df.reindex(df.index.drop("uid"))
    # 変化率
    df_pct_change = df.pct_change()
    # InfをNanで埋める
    df_pct_change = df_pct_change.replace([np.inf, -np.inf], np.nan)
    df_pct_change = df_pct_change.T

    # 外れ値除去
    for i in range(len(df_pct_change.columns)):
        # 列を抽出
        col = df_pct_change.iloc[:, i]
        # 平均と標準偏差
        average = np.mean(col)
        sd = np.std(col)
        # 外れ値の基準点
        outlier_min = average - (sd) * 2
        outlier_max = average + (sd) * 2
        # 範囲から外れている値を除く
        col[col < outlier_min] = None
        col[col > outlier_max] = None
    df_pct_change = df_pct_change.T

    # 傾き
    df_inclination = df_pct_change + 1

    row = len(df)  # 39

    mu_i_list = []  # 各月の各顧客の変化率mu_iのリスト
    mu_i_value_list = []
    for j in range(len(df_inclination)):  # 39
        mui_list = []
        mui_value_list = []
        # 顧客全体の平均変化率
        df_m = df_inclination[row - (j + 1):row - j]
        # infとnanをでnanで埋める
        df_m = df_m.replace(np.inf, np.nan).fillna(np.nan)
        # 平均変化率
        mu_a = df_m.mean(axis=1)
        # 顧客全体の平均変化率
        mu_a = mu_a[0]

        for i in range(len(df_inclination.columns)):  # 459
            # 各顧客の変化率
            mu_i = df_inclination.iloc[row - (j + 1), i]
            if mu_a <= mu_i:  # 閾値: 顧客全体の平均変化率
                # if 1 <= mu_i:  # 閾値: 変化率1
                # print(mu_i)
                mui_list.append(1)
                mui_value_list.append(mu_i)
            elif mu_a > mu_i:  # 閾値: 顧客全体の平均変化率
                # elif 1 > mu_i:  # 閾値: 変化率1
                # print(mu_i)
                mui_list.append(0)
                mui_value_list.append(mu_i)
            else:
                mui_list.append(np.nan)
                mui_value_list.append(np.nan)

        mu_i_list.append(mui_list)
        mu_i_value_list.append(mui_value_list)  # 変化率

    df_mu_i_list = pd.DataFrame(mu_i_list)
    df_mu_i_value_list = pd.DataFrame(mu_i_value_list)  # 変化率

    # print(df_mu_i_list)
    # print(df_mu_i_value_list)  # 変化率
    # df_mu_i_value_list.to_csv("./df_mu_i_value_list.csv")
    return df_mu_i_list, df_mu_i_value_list, df_pct_change


# はじめて1が現れるindex番号を取得
def _month_distance(df_mu_i_list, df):
    df1 = df_mu_i_list
    df_columns = df.columns
    df_index = df["uid"].values

    # 空のデータフレーム
    df2 = pd.DataFrame(
        df1,
        index=df_index,
        columns=df_columns)

    df2 = df2.drop("uid", axis=1)
    df2 = df2.T
    df2 = df2.iloc[::-1]

    for i in range(len(df1)):  # 39
        for j in range(len(df2.columns)):  # 459
            df2.iloc[i, j] = df1.iloc[i, j]
    df3 = df2

    return df3


# Recency：直近購買月
def _recency(df3):
    df_t = df3
    # NAを0に変換
    df_t = df_t.fillna(0)

    latest_purchase = []
    for j in range(len(df_t.columns)):  # 459
        # print(df_t.iloc[i, j])
        # 列名の参照
        col_name = df_t.columns[j]
        # 行名の取得
        month = list(df_t.query('{} == "1.0"'.format(col_name)).index)

        lp = []
        # 直近購買日(リストの0番目)を取得, 直近購買日がない場合はそのまま
        if len(month) > 0:
            month = month[0]
            lp.extend([col_name, month])
        elif len(month) == 0:
            lp.append(col_name)
            pass
        latest_purchase.append(lp)

    df_lp = pd.DataFrame(latest_purchase, columns=["uid", "recency"])
    df_lp["recency"] = pd.to_datetime(df_lp["recency"])

    # 基準日
    NOW = int(dt.datetime(2019, 7, 1).strftime('%s'))

    # unixtimeに変換
    df_lp['recency'] = pd.to_datetime(df_lp['recency']).dt.tz_localize('Asia/Tokyo')
    df_lp['recency'] = df_lp['recency'].astype('int64') // 10 ** 9

    # uidを元にグループ化して、集計を行う
    df_recency = df_lp.groupby('uid').agg({'recency': lambda x: int((NOW - x.max()) / 86400)})  # 最新購入日

    # 中央値
    time_median = df_recency['recency'].median()
    # 最大値
    time_max = df_recency['recency'].max()
    # 最小値
    time_min = df_recency['recency'].min()
    # print(time_max)
    # 第三四分位点, 第一四分位点
    recency_q75, recency_q25 = np.percentile(df_recency['recency'], [75, 25])
    return df_recency, time_median, time_max, time_min, recency_q75, recency_q25


# Frequency：契約月数
def _frequency(df):
    # 0以外の値をカウントするためdfを転置
    df_zero = (df.T != 0)
    # 0行目(uid)を除く行において,0以外の個数(売上がある月)をカウント
    df_zero_count = df_zero.iloc[1:, :].sum()
    # カラムを追加
    df_frequency = pd.DataFrame(df_zero_count, columns=['frequency'])
    # uid, total_monetary列を結合
    df_frequency = pd.concat([df["uid"], df_frequency], axis=1)
    return df_frequency


# Monetary：購買額
def _monetary(df):
    # 空のデータフレーム
    df_monetary = pd.DataFrame()
    # 顧客ごとの購入金額合計
    df_monetary['monetary'] = df.sum(axis=1)
    # uid, total_monetary列を結合
    df_monetary = pd.concat([df["uid"], df_monetary], axis=1)
    # 中央値
    amount_median = df_monetary['monetary'].median()
    # 最大値
    amount_max = df_monetary['monetary'].max()
    # 最小値
    amount_min = df_monetary['monetary'].min()
    # 第三四分位点, 第一四分位点
    monetary_q75, monetary_q25 = np.percentile(df_monetary['monetary'], [75, 25])

    return df_monetary, amount_median, amount_max, amount_min, monetary_q75, monetary_q25


# uid,R,F,Mを結合
def _merge_rfm(df_recency, df_frequency, df_monetary):
    df_fm = pd.merge(df_frequency, df_monetary, on='uid')
    df_rfm = pd.merge(df_recency, df_fm, on='uid')
    return df_rfm


# ランク分け
def _divide_ranks(df_rfm):
    # --- Recency：直近購買月 ---
    # 最大値
    recency_max = df_rfm['recency'].max()
    # 中央値
    recency_median = df_rfm['recency'].median()
    # 最小値
    recency_min = df_rfm['recency'].min()
    # 第三四分位点, 第一四分位点
    recency_q75, recency_q25 = np.percentile(df_rfm['recency'], [75, 25])
    # --- ランク分け ---
    # ランク3:
    df_rfm.loc[(df_rfm['recency'] >= recency_min) & (
            df_rfm['recency'] <= recency_q25), 'R'] = 3
    # ランク2:
    df_rfm.loc[(df_rfm['recency'] > recency_q25) & (
            df_rfm['recency'] <= recency_q75), 'R'] = 2
    # ランク1:
    df_rfm.loc[(df_rfm['recency'] > recency_q75) & (
            df_rfm['recency'] <= recency_max), 'R'] = 1

    # --- Frequency：契約月数 ---
    # 最大値
    months_max = df_rfm['frequency'].max()
    # 中央値
    months_median = df_rfm['frequency'].median()
    # 最小値
    months_min = df_rfm['frequency'].min()
    # 第三四分位点, 第一四分位点
    frequency_q75, frequency_q25 = np.percentile(df_rfm['frequency'], [75, 25])
    # --- ランク分け ---
    # ランク3:
    df_rfm.loc[(df_rfm['frequency'] >= frequency_q75) & (
            df_rfm['frequency'] <= months_max), 'F'] = 3
    # ランク2:
    df_rfm.loc[(df_rfm['frequency'] >= frequency_q25) & (
            df_rfm['frequency'] < frequency_q75), 'F'] = 2
    # ランク1:
    df_rfm.loc[(df_rfm['frequency'] >= months_min) & (
            df_rfm['frequency'] < frequency_q25), 'F'] = 1

    # --- Monetary：購買額 ---
    # 最大値
    monetary_max = df_rfm['monetary'].max()
    # 中央値
    monetary_median = df_rfm['monetary'].median()
    # 最小値
    monetary_min = df_rfm['monetary'].min()
    # 第三四分位点, 第一四分位点
    monetary_q75, monetary_q25 = np.percentile(df_rfm['monetary'], [75, 25])
    # --- ランク分け ---
    # ランク3:
    df_rfm.loc[(df_rfm['monetary'] >= monetary_q75) & (
            df_rfm['monetary'] <= monetary_max), 'M'] = 3
    # ランク2:
    df_rfm.loc[(df_rfm['monetary'] >= monetary_q25) & (
            df_rfm['monetary'] < monetary_q75), 'M'] = 2
    # ランク1:
    df_rfm.loc[(df_rfm['monetary'] >= monetary_min) & (
            df_rfm['monetary'] < monetary_q25), 'M'] = 1

    df_RFM = df_rfm.loc[:, ['R', 'F', 'M']]

    # スコアトータル
    rfm_total_score = pd.DataFrame(df_RFM.sum(axis=1), columns=["rfm_total_score"])
    # uid, rfm_total_score列を結合
    df_rfm_total = pd.concat([df_rfm["uid"], rfm_total_score], axis=1)
    # df_rfmに結合
    df_rfm = pd.merge(df_rfm, df_rfm_total, on='uid')
    # csv出力
    df_rfm.to_csv("./rfm_test.csv")
    return df_rfm


# DB格納
def _insert_db(df_rfm):
    table_name = "rfm_result"
    db_settings = {"host": "127.0.0.1",
                   "database": "RFM_DB",
                   "user": "root",
                   "password": "password",
                   "port": 3306
                   }
    engine = create_engine('mysql://{user}:{password}@{host}:{port}/{database}'.format(**db_settings))
    # df_rfm.to_sql(table_name, engine, flavor='mysql', index=False)
    df_rfm.to_sql(table_name, engine, index=False)


def main():
    df = _read_csv()
    df_mu_i_list, df_mu_i_value_list, df_pct_change = _rate_of_change(df)
    df3 = _month_distance(df_mu_i_list, df)
    df_recency, time_max, time_median, time_min, recency_q75, recency_q25 = _recency(df3)
    df_frequency = _frequency(df)
    df_monetary, amount_max, amount_median, amount_min, monetary_q75, monetary_q25 = _monetary(df)
    df_rfm = _merge_rfm(df_recency, df_frequency, df_monetary)
    df_rfm = _divide_ranks(df_rfm)


if __name__ == '__main__':
    main()
